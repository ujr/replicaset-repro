# Repro for failure to reconnect to MongoDB replica set

* start 3 instances of MongoDB (I used 4.4.17) with configs supplied for convenience, like
bin\mongod.exe -f mongodb/mongod1.cfg

* create replica set according to https://www.mongodb.com/docs/rapid/tutorial/deploy-replica-set-for-testing/

* start repro by `node index.mjs`

* log on to primary (`mongosh localhost:27017`, whatever), step down primary `rs.stepDown()`
  (maybe this needs to be done a second or third time - bug may be a race)
