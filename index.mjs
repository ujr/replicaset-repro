import { inspect } from 'node:util'
import os from 'node:os'
import process from 'node:process'

import { MongoClient } from 'mongodb'
import Pino from 'pino'

const logger = Pino()
function log(...args) {
    logger.info(...args)
}

function error(...args) {
    logger.error(...args)
}

const dbName = 'somedb'
const hostname = os.hostname().toLowerCase()
//const hostname = 'localhost'
const replicaSetName = 'rs0'
const connectionURI = `mongodb://${hostname}:27017,${hostname}:27018,${hostname}:27019/${dbName}?replicaSet=${replicaSetName}`
const maxClients = 3
const warmupDocsMax = 9999
let warmupPromise

const clients = []

for (let i = 0; i < maxClients; i++) {
    const appName = `test-service-${i}`
    const client = new MongoClient(connectionURI, {
        appName,
        //family: 4,
        //readPreference: 'primary', // 'primaryPreferred',
        serverSelectionTimeoutMS: 15000,
    })

    client.on('serverHeartbeatStarted', (data) => {
        //
    })
    client.on('serverHeartbeatSucceeded', (data) => {
        //
    })
    client.on('serverHeartbeatFailed', (data) => {
        //
    })
    client.on('serverDescriptionChanged', (data) => {
        //
    })
    client.on('topologyDescriptionChanged', (data) => {
        log(
            '%s: %j',
            appName,
            inspect(data.newDescription, { depth: Infinity, showHidden: false })
        )
    })

    function createUserDoc() {
        return { name: 'new user', data: 'some Data '.repeat(50), moredata: 'more Info'.repeat(50) }
    }

    async function warmup() {
        const db = await client.db(dbName)
        const usersCollection = db.collection('users')
        let count = await usersCollection.countDocuments()
        const users = []
        while (count < warmupDocsMax) {
            users.push(createUserDoc())
            count++
        }
        if (users.length > 0) {
            await usersCollection.insertMany(users)
        }
    }
    
    client.connect().then(async () => {
        if (!warmupPromise) {
            warmupPromise = warmup()
        }
        await warmupPromise

        log(`${appName}: getting started`)
        const db = await client.db(dbName)

        setInterval(async () => {
            try {
                const usersCollection = db.collection('users')
                const users = await usersCollection.find().toArray()
                const count = await usersCollection.countDocuments()

                await usersCollection.insertOne(createUserDoc())
            } catch (e) {
                error("%s - %j", appName, inspect(e))
                process.exit(1)
            }
        }, 250 * (1 + i))
    })

    clients.push(client)
}
